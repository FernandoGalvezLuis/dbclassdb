create database if not exists Fernando_Individual_Project;
use Fernando_Individual_Project;


CREATE TABLE if not exists `Fernando_Individual_Project`.`Resume_Entry` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ocupationName` VARCHAR(1024) NOT NULL,
  `startingEndingDate` VARCHAR(1024) NOT NULL,
  `companyNameLocation` VARCHAR(1024) NOT NULL,
  `description` VARCHAR(1024) NOT NULL,  
  PRIMARY KEY (`id`)
  )
  ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
  
  CREATE TABLE if not exists `Fernando_Individual_Project`.`incoming_message` (
  `message_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(1024) NOT NULL,
  `email` VARCHAR(1024) NOT NULL,
  `message_content` VARCHAR(1024) NOT NULL,
    
  PRIMARY KEY (`message_id`)
  )
  ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
  
  USE mysql;

CREATE USER 'FernandoIndividualProject'@'localhost' IDENTIFIED WITH mysql_native_password BY '123456';

GRANT ALL PRIVILEGES ON *.* TO 'FernandoIndividualProject'@'localhost';

flush privileges;